module.exports = {
  content: ['./build/**/*.html'],
  theme: {
    extend: {
      fontFamily: {
        /* https://modernfontstacks.com/#geometric-humanist */
        sans: [
          'Avenir',
          'Montserrat',
          'Corbel',
          'URW Gothic',
          'source-sans-pro',
          'sans-serif',
        ],
      },
    },
  },
  plugins: [],
};
