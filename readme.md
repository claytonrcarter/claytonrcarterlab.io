# My site and blog

In which nothing valuable is contained.

## Architecture

A minimal SSG that _I_ can reason about and pick back up after long hiatuses
without having to search around for how to use it. Relys heavily on bash, pipelines and core unix commands. Pandoc does the heavily lifting. SQLite is used during build for the blog index and tags.

### TODO

- [ ] _maybe_ list updated at in recent posts, blog index, tax indices
- [ ] _maybe_ show an excerpt/abrtract in recent posts
- [ ] `<meta name="description" content="${if(it.meta_description )} ${it.meta_description} ${else} ${siteDescription} ${endif}"/>`
- [ ] `<meta property="og:description" content="${siteDescription}" />`
- [ ] try to hack support for 1 line arrays in YAML front matter (eg tags, updated_at are all 1 element/line but would be nice to squish onto 1 line)
- [ ] colorize the build script output w/ ANSI colors
- [ ] consider replacing `for POST in _posts/*.md` with `POSTS=(find _posts)` and using `-newermt "-2 seconds"` to find only the new posts in WATCH mode
  - [ ] ditto for pages
- [ ] better date parsing? (ie it's all just "2024-04-01" now)
- [ ] read time/word counts to posts? (exlude YAML and code blocks)
- [ ] change `master` to `main`
- [ ] cache busting for the CSS file?

## Overview

`_assets` need to be build/compiled before use (eg Tailwind)
`_pages` are rendered as stand alone pages
`_posts` are rendered as a blog posts, with an index, tags and RSS
`_templates` are used to render the above
`assets` is included as is
`build` is where the rendered files are stored

### Example

This example site structure

```
_pages
    index.md
    experience.md
    about.md
_posts
    post1.md
    post2.md
    draft.md
_templates
    _main.html
    _page.html
    _post.html
assets
    photo.jpg
    styles.css
_assets
    tailwind.css
```

would render as (where `/` is short for `/index.html`, and all files are in `build` dir)

```
/
experience/
about/
photo.jpg
styles.css
tailwind.css
posts/
    post1/
    post2/
    feed.xml
```

## HOWTO

- `bin/build.sh` - just build
- `bin/serve.sh` - serve public to localhost:8080
- `bin/setup.sh` - install dependencies

## Resources

- https://pandoc.org/MANUAL.html#templates
- https://pandoc.org/MANUAL.html#extension-yaml_metadata_block
- https://www.romangeber.com/static_websites_with_pandoc/ (inc custom variables)
- https://github.com/adityaathalye/shite
- https://rsdoiel.github.io/blog/2020/11/09/Pandoc-Partials.html
- https://pandoc.org/MANUAL.html#partials
- pandoc custom vars: https://superuser.com/questions/901517/how-can-i-get-pandoc-to-recognize-my-custom-yaml-fields
- default html template: https://github.com/jgm/pandoc-templates/blob/master/default.html5
- ref on complex templates via piping: https://stackoverflow.com/questions/14249811/markdown-to-docx-including-complex-template
- a simple PHP based SSG that uses `wget` for rendering: https://hackernoon.com/the-simplest-static-site-generator-c775ed88d15a
- pipe commands to a bg process in bash: https://unix.stackexchange.com/questions/384159/how-to-pipe-commands-to-a-process-running-in-the-background
