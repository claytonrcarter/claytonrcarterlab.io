<?php

// This is the simple long polling version. It is paired with some JS in the
// main.html template.

header('Content-Type: application/json');
header('Cache-Control: no-cache');

$updated = [];
exec('find ' . __DIR__ . ' -type f -newermt "-2 seconds"', $updated);

echo json_encode(
    array_map(function ($s) {
        $s = str_replace([__DIR__, '/index.html'], '', $s);
        return $s === '' ? '/' : $s;
    }, $updated)
);

exit();

// This is the (disabled) SSE version that never worked b/c the PHP dev server
// blocks and locks, even with PHP_CLI_SERVER_WORKERS=4. This would need to be
// paired with some JS (below) on the front end.

// const eventSource = new EventSource("/hot-reload.php");
// eventSource.onmessage = (event) => {
//     const changes = JSON.parse(event.data);
//     if (
//         changes.includes(window.location.pathname) ||
//         changes.includes('/styles.css')
//     ) {
//         window.location.reload();
//     }
// };

// Ref: https://kevinchoppin.dev/blog/server-sent-events-in-php
// Ref: https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events/Using_server-sent_events#sending_events_from_the_server

ignore_user_abort(false);

header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

ob_start();
// just for debugging
echo 'retry: 2000' . "\n\n";

while (true) {
    $updated = [];
    exec('find ' . __DIR__ . ' -type f -newermt "-1 seconds"', $updated);

    if (count($updated)) {
        $updated = json_encode(
            array_map(function ($s) {
                $s = str_replace([__DIR__, '/index.html'], '', $s);
                return $s === '' ? '/' : $s;
            }, $updated)
        );
        echo "data: {$updated}\n\n";
    }
    ob_flush();
    flush();

    if ($connection_status() !== CONNECTION_NORMAL) {
        break;
    }

    sleep(1);
}
