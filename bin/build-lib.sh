function finish {
    # clean up temp filed
    rm "$DB"
    rm build/_*
}


# Simplistic YAML parser, suitable for simple front matter. Only has to handle
# key/value and array elements.
function yaml_to_bash {
    echo "$1" |
        awk '
        # skip comments
        /^#/ {
            next;
        }

        # first field is a prop name, save it
        $1 ~ /[a-z]+:$/ {
            name = substr($1, 0, length($1)-1)
        }

        {
            # is this an array element, or single key/value
            if ($1 == "-") {
                assign = "+="
            } else {
                assign = "="
            }
        }

        # if there is a value in the second field, format and print it
        $2 != "" {
            $1=""
            printf("FRONT_MATTER_%s%s(\"%s\")\n", name, assign, substr($0, 2))
        }
    '
}

# build a page (for special cases)
# the page is built directly from the input file into the output file via a
# single template, eg _posts.md -> rss.xml -> feed.atom
function build_page_direct {
    _INFILE="$1"
    _OUTFILE="$2"
    _TEMPLATE="$3"
    _URL="$4"
    _REST=("${@:5}")

    if [[ "${_REST[@]}" =~ " " ]]
    then
        # $_REST can be quoted below if all calls to build_page_direct include
        # addl args; otherwise you will need to figure out how to handle cases
        # where some params include spaces and some are not present
        echo "BUG: build_page_direct doesn't support args containing spaces"
        exit
    fi

    pandoc "$_INFILE" \
        --to=html \
        --output="$_OUTFILE" \
        --template="$_TEMPLATE" \
        --standalone \
        --variable=siteName:"$SITE_NAME" \
        --variable=siteBaseUrl:"$SITE_BASE_URL" \
        --variable=siteEmail:"$SITE_EMAIL" \
        --variable=siteAuthor:"$SITE_AUTHOR" \
        --variable=pageUrl:"$_URL" \
        --variable=year:"$(date +%Y)" \
        $_REST
}

# build a page
# the page is built by passing a file through 2 templates
# eg post.md -> _post.html -> main.html -> post/index.html
function build_page {
    _INFILE="$1"
    _OUTFILE="${2}/index.html"
    _INTEMPLATE="$3"
    _OUTTEMPLATE=_templates/main.html
    _TITLE="$4"
    _URL="$5"
    _REST=("${@:6}")

    if [[ -z "${_REST[@]}" ]]
    then
        # $_REST is quoted below because all calls to build_page included
        # addl args; if that's not the case now, then it will be passing an
        # empty arg ("") to pandoc
        echo "BUG: build_page assumes that addl args will be passed"
        exit
    fi


    pandoc "$_INFILE" \
        --to=html \
        --template="$_INTEMPLATE" \
        --standalone \
        --variable=siteName:"$SITE_NAME" \
        --variable=siteBaseUrl:"$SITE_BASE_URL" \
        --variable=siteEmail:"$SITE_EMAIL" \
        --variable=siteAuthor:"$SITE_AUTHOR" \
        --variable=pageUrl:"$_URL" \
        "${_REST[@]}" |
    pandoc --from=html \
        --output="$_OUTFILE" \
        --template="$_OUTTEMPLATE" \
        --standalone \
        --variable=siteName:"$SITE_NAME" \
        --variable=siteBaseUrl:"$SITE_BASE_URL" \
        --variable=siteEmail:"$SITE_EMAIL" \
        --variable=siteAuthor:"$SITE_AUTHOR" \
        --variable=pageUrl:"$_URL" \
        --variable=title:"$_TITLE" \
        --variable=currentPage:"$FILENAME" \
        --variable=year:"$(date +%Y)" \
        $WATCH \
        "${_REST[@]}"
}
