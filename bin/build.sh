#!/usr/bin/env bash

##
## PARSE ARGS
##
DRAFT=""
PROD=""
WATCH=""
for ARG in "$@"; do
    case $ARG in
        --draft) DRAFT="draft";;
        --prod) PROD="prod";;
        --watch) WATCH="--variable=watch:true";;
        -h | --help)
        echo "build.sh: build the site and blog"
        echo "  --draft: build draft blog posts"
        echo "  --prod:  do extra processing for production"
        echo "  --watch: rebuild (and reload) files as they are updated"
        exit 0;;
        *) echo "ERROR: unknown arg '$ARG'"; exit 1;;
    esac
done

##
## CONFIG
##
SITE_NAME="Danan Consulting"
SITE_BASE_URL="https://danan.dev"
SITE_EMAIL="clayton@danan.dev"
SITE_AUTHOR="Clayton Carter"
DB=build/db.sqlite3

if [ ! "$PROD" ]; then
    SITE_BASE_URL=""
fi

##
## SCRIPT SETUP
##
. $(dirname ${BASH_SOURCE[0]})/build-lib.sh
trap finish EXIT

##
## BUILD SETUP
##
echo Prepping build dir...
rm -rf build
mkdir build
echo 'CREATE TABLE posts(file, path, title, abstract, published)' | sqlite3 "$DB"
echo 'CREATE TABLE tags(file, tag, path)' | sqlite3 "$DB"
echo 'CREATE UNIQUE INDEX posts_unique_file ON posts(file)' | sqlite3 "$DB"
echo 'CREATE UNIQUE INDEX tags_unique_file_tag ON tags(file, tag)' | sqlite3 "$DB"

##
## STATIC FILES & ASSETS
##
echo Copying static assets...
if [ -d assets ]; then
    cp -R assets/* build
fi

while true; do
_NEED_REGEN=""
_FORCE_REGEN=$(find _templates -type f -newermt "-2 seconds")

##
## BLOG POSTS
##
if [ "$_FORCE_REGEN" -o ! -d "build/blog" ]; then
    # don't reprint every time in watch mode
    echo Building posts...
fi
for POST in _posts/*.md; do
    # skip the post template and draft posts
    if [ "$POST" == _posts/_template.md ] \
        || [ ! "$DRAFT" ] \
        && ! grep -E '^published: .+$' "$POST" >/dev/null
    then
        continue
    fi

    FILENAME=$(basename "$POST" | cut -d. -f1)
    if [ "$FILENAME" == "index" ]; then
        echo ERROR blog index is generated automatically from _blog-index partial
        exit 1
    fi

    YAML=$(awk 'BEGIN { RS = "---" } NR==2' "$POST")
    eval "$(yaml_to_bash "$YAML")"

    if [ ! "$DRAFT" -a -z "$FRONT_MATTER_published" ]; then
        echo "    " skipping draft post
        continue
    fi

    if [ -n "$FRONT_MATTER_slug" ]; then
        PUBLIC_DIR=blog/"$FRONT_MATTER_slug"
    else
        PUBLIC_DIR=blog/"$FILENAME"
    fi
    OUT_DIR=build/"$PUBLIC_DIR"

    if [ ! "$_FORCE_REGEN" -a ! "$POST" -nt "$OUT_DIR"/index.html ]; then
        continue
    fi

    # awk removes front matter and code blocks; tr trims the wc output
    WORDCOUNT=$( \
        awk '/^[-`]{3}/ {skip=!skip; next} !skip { print }' $POST |
        wc -w |
        tr -d ' ' \
    )
    echo "   Building ${FILENAME}... ($WORDCOUNT words)"
    _NEED_REGEN="regen"
    mkdir -p "$OUT_DIR"

    # Convert the post and send it through the post template, then send that
    # through the main template.
    build_page "$POST" "$OUT_DIR" \
        _templates/_post.html \
        "$FRONT_MATTER_title" \
        "$SITE_BASE_URL/$PUBLIC_DIR" \
        --variable=is_blog:true \
        --variable=is_post:true

    sqlite3 "$DB" <<EOF
INSERT INTO posts
VALUES(
    '$POST',
    '$PUBLIC_DIR',
    '$FRONT_MATTER_title',
    '$FRONT_MATTER_abstract',
    '$FRONT_MATTER_published'
) ON CONFLICT(file) DO UPDATE SET
    path = excluded.path,
    title = excluded.title,
    abstract = excluded.abstract,
    published = excluded.published
EOF

    for tag in "${FRONT_MATTER_tags[@]}"; do
        echo "
INSERT INTO tags VALUES('$POST', '$tag', '$PUBLIC_DIR')
ON CONFLICT(file, tag) DO UPDATE SET
    path = excluded.path
" | sqlite3 "$DB"
    done

    # unset all front matter vars for next iteration
    # NB ignore shellcheck warning
    unset $(compgen -A variable -X '!FRONT_MATTER_*')
done

##
## BLOG TAGS, INDEX & RSS
##
if [ "$_FORCE_REGEN" -o "$_NEED_REGEN" ]; then
    ##
    ## TAGS
    ##
    echo Building tag indices...

    # NB ignore shellcheck warning
    TAGS=($(echo "SELECT DISTINCT tag FROM tags" | sqlite3 "$DB"))
    for tag in "${TAGS[@]}"; do
        echo "  " Building "$tag"...

        echo "---
title: suppress warning
posts:" >build/_posts.md
        sqlite3 "$DB" >>build/_posts.md <<EOF
SELECT printf(
"- title: %s
  path: %s
  published: %s
", title, posts.path, published)
FROM posts
WHERE path in (SELECT path FROM tags WHERE tags.tag = '$tag')
ORDER BY published DESC
EOF
        echo "---" >>build/_posts.md

        PUBLIC_DIR=blog/tags/"$tag"
        OUT_DIR=build/"$PUBLIC_DIR"
        mkdir -p "$OUT_DIR"

        build_page build/_posts.md "$OUT_DIR" \
            _templates/_blog-index.html \
            "Tag: $tag | $SITE_NAME" \
            "$SITE_BASE_URL/$PUBLIC_DIR" \
            --variable=is_blog:true \
            --variable=tag:"$tag"
    done

    ##
    ## BLOG INDEX
    ##
    echo Building blog index...
    echo "---
title: suppress warning
posts:" >build/_posts.md
    sqlite3 "$DB" >>build/_posts.md <<EOF
SELECT printf(
"- title: %s
  abstract: %s
  path: %s
  published: %s
", title, abstract, path, published)
FROM posts
ORDER BY published DESC
EOF
    echo "---" >>build/_posts.md

    build_page build/_posts.md build/blog \
        _templates/_blog-index.html \
        "Blog | $SITE_NAME" \
        "$SITE_BASE_URL/blog" \
        --variable=is_blog:true

    ##
    ## RSS
    ##
    echo Building RSS...

    # NB: use html as an XMLish format for escaping
    build_page_direct build/_posts.md \
        build/blog/feed.atom \
        _templates/rss.xml \
        "$SITE_BASE_URL/blog/feed.atom" \
        --variable=atomDate:"$(date -Iseconds)" # Atom Date is ISO-8601 w/ seconds
fi

##
## PAGES
##
if [ "$_FORCE_REGEN" -o ! -f "build/index.html" ]; then
    # don't reprint every time in watch mode
    echo Building pages...
fi
for PAGE in _pages/*.html; do
    FILENAME=$(basename "$PAGE" | cut -d. -f1)
    PUBLIC_DIR="$FILENAME"

    if [ "$FILENAME" == "index" ]; then
        OUT_DIR=build
        TITLE="$SITE_NAME"
    else
        OUT_DIR=build/"$PUBLIC_DIR"
        # Uppercase the first letter of the file name in the title
        TITLE=$(echo "$FILENAME | $SITE_NAME" | awk '{print toupper(substr($0, 0, 1)) substr($0, 2)}')
    fi

    if [ ! "$PAGE" -nt "$OUT_DIR"/index.html ]; then
        continue
    fi

    echo "  " Building "$FILENAME"...
    _NEED_REGEN="regen"

    if [ "$FILENAME" == "index" ]; then
        # handle the "recent posts" feature
        # compile the base template in an intermediate template
        echo "---
title: suppress warning
recent_posts:" >build/_recents.md
        sqlite3 "$DB" >>build/_recents.md <<EOF
SELECT printf(
"- title: %s
  path: %s
  published: %s
", title, posts.path, published)
FROM posts
ORDER BY published DESC
LIMIT 5
EOF
        echo "---" >>build/_recents.md

        build_page_direct build/_recents.md \
            "$OUT_DIR"/_index.html \
            "$PAGE" \
            "$SITE_BASE_URL/$PUBLIC_DIR"

        # then feed the intermediate template through the normal pipeline
        # echo "<head><title>suppress</title></head>" > "$OUT_DIR"/_index.html
        # cat "$OUT_DIR"/__index.html >> "$OUT_DIR"/_index.html
        PAGE="$OUT_DIR"/_index.html
    fi

    mkdir -p "$OUT_DIR"

    # Convert the page and send it through the page template, then send that
    # through the main template.
    build_page "$PAGE" "$OUT_DIR" \
        _templates/_page.html \
        "$TITLE" \
        "$SITE_BASE_URL/$PUBLIC_DIR" \
        --metadata=title:"$TITLE" \
        --variable=is_"$FILENAME":true

    # --template=$MY_PATH/template.html \
    # --css "/css/milligram.min.css" \
    # --css "/css/custom.css" \
    # --variable=lastUpdated:$( stat -c %y index.md | cut -f 1 -d ' ' ) \
    # --variable=creationDate:$( stat -c %w index.md | cut -f 1 -d ' ' )
done

if [ "$_FORCE_REGEN" -o "$_NEED_REGEN" ]; then
    echo Building CSS...
    MAYBE_MINIFY=""
    if [ "$PROD" ]; then
        MAYBE_MINIFY="--minify"
    fi
    ./tailwindcss -i _assets/styles.css -o build/styles.css $MAYBE_MINIFY

    if [ "$PROD" -a -f ./node_modules/.bin/prettier ]; then
        echo Running prettier...
        # only run on HTML to avoid de-minifying CSS
        ./node_modules/.bin/prettier \
            --write \
            --ignore-path=disable-gitignore-exclusion \
            "./build/**/*.html" > /dev/null
    fi
fi

if [ "$WATCH" ]; then
    if [ "$_FORCE_REGEN" -o "$_NEED_REGEN" ]; then
        echo "Watching..."
    fi
    sleep 1
else
    exit 0
fi

done # while true
