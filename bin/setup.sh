#!/usr/bin/env sh

if [ "$(uname)" = "Darwin" ]; then

    # brew install pandoc sqlite3 node prettier

    curl -sL \
        --output tailwindcss \
        https://github.com/tailwindlabs/tailwindcss/releases/latest/download/tailwindcss-macos-x64
    chmod +x tailwindcss

elif [ "$(uname)" = "Linux" ]; then

    # apt install pandoc sqlite3 node prettier

    curl -sL \
        --output tailwindcss \
        https://github.com/tailwindlabs/tailwindcss/releases/latest/download/tailwindcss-linux-x64
    chmod +x tailwindcss

else
    echo ERROR Unsupported system: "$(uname)"
    exit 1
fi
